Rea College Noord Portfolio's
=============================
Een verzamelsite voor portfolio's van de leerlingen van het Rea College in Leeuwarden en Groningen.

Bekijk de live versie [hier](http://www.reacollege.net).


Layout door [Jasper Frumau](http://jfrumau.reacollege.net/), functionaliteit door [Jelmer Schuiteboer](http://jschuiteboer.reacollege.net/)

Installatie
-----------
1. Zet de bestanden op uw webserver.
2. Kopieer en hernoem het bestand `config.php.example` naar `config.php`.
3. Open `config.php` en pas de instellingen naar uw voorkeur aan.
4. Open `cron.php` in uw browser zodat deze het metadata bestand en de thumbnails aanmaakt.
5. Nu kunt u `index.php` openen in uw browser.