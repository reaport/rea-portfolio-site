$(document).ready(function() {
	/* view switching */	
	var switchClass = function(elem, isList) {
		if(isList) {
			elem.addClass('list');
			elem.removeClass('grid');
			$.cookie('view', 'list');
		} else {
			elem.addClass('grid');
			elem.removeClass('list');
			$.cookie('view', 'grid');
		}
	}

	$('#viewType').click(function(e) {
		switchClass($(this), !$('.main').hasClass('grid'));
		switchClass($('.main'), $('.main').hasClass('grid'));
	});
	
	/* back to top */	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 210) {
			$('.backToTop').fadeIn();
		} else {
			$('.backToTop').fadeOut();
		}
	});
	
	$('.backToTop').click(function(){
		$('html, body').animate({scrollTop : 0}, 800);
		return false;
	});
});