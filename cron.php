<?php
	include 'init.php';
	
	$domainManager = new DomainManager();
	$googleApi = new GoogleApi();
	$thumbnailManager = new ThumbnailManager();
	
	
	// init database
	$db = Database::getInstance($config['DB_DSN'], $config['DB_USER'], $config['DB_PASS'], $config['DB_OPTIONS']);
	$result = $db->query(
		'SELECT contact_name, customer_no, domain FROM client ' .
		'JOIN sys_user ON client.client_id=sys_user.client_id ' .
		'JOIN web_domain ON sys_user.userid=web_domain.sys_userid');
	
	foreach($result as $row) {
		// we only need the fist letter from customer_no.
		$row['customer_no'] = substr($row['customer_no'], 0, 1);
		
		// ignore the row if customer_no is not a 'G' or an 'L'
		if($row['customer_no'] !== 'G' && $row['customer_no'] !== 'L') {
			continue;
		}
		
		// get the bit before the first dot
		$row['domain_name'] = strstr($row['domain'], '.', true);
		
		// ensure url scheme
		if(parse_url($row['domain'], PHP_URL_SCHEME) === null) {
			$row['domain'] = 'http://' . $row['domain'];
		}
				
		$googleApi->makeRequest($row['domain']);
		$thumbnailManager->setThumbnail($row['domain_name'], $googleApi->getThumbnail());
		
		$domain = new Domain();
		$domain->setName($row['domain_name']);
		$domain->setUrl($row['domain']);
		$domain->setClientName($row['contact_name']);
		$domain->setLocation(Location::fromString($row['customer_no']));
		$domain->setPageTitle($googleApi->getPageTitle());
		$domain->setThumbnail($config['THUMBNAIL_DIR'] . $row['domain_name'] . '.jpg');
		
		$domainManager->setDomain($domain);
	}
	
	$thumbnailManager->saveScreenShots($config['THUMBNAIL_DIR']);
	$domainManager->saveDomainsToFile($config['METADATA_FILE']);