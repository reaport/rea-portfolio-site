<?php
	include 'init.php';
	
	// get the id
	$domainName = Request::get('domain');
	
	if($domainName == null) {
		header('HTTP/1.0 404 Not Found');
		die('404 not found');
	}
	
	// get the domain
	$domainManager = new DomainManager();
	$domainManager->loadDomainsFromFile($config['METADATA_FILE']);
	$domain = $domainManager->getDomainFromName($domainName);
	
	if($domain == null) {
		header('HTTP/1.0 404 Not Found');
		die('404 not found');
	}
	
	// render the template
	$template = new Template();
	$template->domainUrl = $domain->getUrl('url');
	
	echo $template->render('template/navFrame.html');