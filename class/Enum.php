<?php
	// from http://www.whitewashing.de/2009/08/31/enums-in-php.html
	abstract class Enum {
		final public function __construct($value) {
			$c = new ReflectionClass($this);
			if(!in_array($value, $c->getConstants())) {
				throw new InvalidArgumentException("'". $value .'\' is not a valid enum value');
			}
			$this->value = $value;
		}
		
		public function __toString() {
			return (string) $this->value;
		}
	}