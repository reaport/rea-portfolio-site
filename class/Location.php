<?php

class Location extends Enum {
	const UNKNOWN = 0;
	const LEEUWARDEN = 1;
	const GRONINGEN = 2;
	
	public static function fromString($locString) {
		switch(strtolower($locString)) {
			case 'leeuwarden':
			case 'l':
			case '1':
				return new Location(Location::LEEUWARDEN);
			case 'groningen':
			case 'g':
			case '2':
				return new Location(Location::GRONINGEN);
		}
		
		return new Location(Location::UNKNOWN);
	}
	
	public function __toString() {
		switch($this->value) {
			case static::LEEUWARDEN:	return 'Leeuwarden';
			case static::GRONINGEN: 	return 'Groningen';
			default:					return 'Onbekend';
		}
	}
}