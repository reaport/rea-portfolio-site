<?php

class Template {
	public $properties = array();
	
	public function __set($key, $value) {
		$this->properties[$key] = $value;
	}
	
	public function __get($key) {
		//return $this->properties[$key];
		return isset($this->properties[$key]) ? $this->properties[$key] : null;
	}
	
	public function render($templateFile) {			
		if(!file_exists($templateFile)) {
			throw new Exception('template file not found: \'' . $templateFile . "'");
		}
		
		ob_start();
		include $templateFile;
		return ob_get_clean();
	}
}