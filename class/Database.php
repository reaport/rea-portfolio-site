<?php
	class Database {
		private static $instance;
		
		public static function getInstance($dbDsn, $dbUser, $dbPass, $dbOptions) {
			if(!static::$instance) {
				static::$instance = new PDO($dbDsn, $dbUser, $dbPass, $dbOptions);				
			}
			
			return static::$instance;
		}
	}