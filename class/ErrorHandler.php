<?php
	class ErrorHandler {
		public static function HandleException() {
			// remove previous output from the output buffer
			ob_clean();
			
			// send a http 500 message
			header('HTTP/1.1 500 Internal Server Error');
			
			try {
				$errorPage = new Template();
				
				$menu = new Template();
				$menu->lwdButtonClass = Request::get('loc') == 'L' ? 'pressed' : '';
				$menu->grnButtonClass = Request::get('loc') == 'G' ? 'pressed' : '';
				$menu->query = Request::get('q', '');
				$menu->loc = Request::get('loc', '');
				$menu->viewClass = (Request::cookie('view', 'grid')) == 'grid' ? 'list' : 'grid';
				$errorPage->menu = $menu->render('template/menu.html');
				
				$content = new Template();
				$errorPage->content = $content->render('template/500.html');
				
				echo $errorPage->render('template/base.html');
			} catch(Exception $e) {
				echo '500: Internal Server Error';
			}
		}
	}