<?php
	class GoogleApi {
		private $pageTitle;
		private $thumbnail;
		
		/**
		* Will make a request to the google api. Throws an ErrorException if the request fails.
		*/
		public function makeRequest($url) {
			$response = file_get_contents('https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=' . $url . '&screenshot=true');
			
			if($response === false) {
				// This line should never be triggerd since file_get_contents should throw an error.
				throw new Exception('invalid response');
			}
			
			$response = json_decode($response, true);
			
			// transform the thumbnail data to a format we can use.
			$this->thumbnail = str_replace(array('_','-'), array('/','+'), $response['screenshot']['data']);
			$this->thumbnail = base64_decode($this->thumbnail);
			
			$this->pageTitle = $response['title'];
		}

		public function getPageTitle() {
			return $this->pageTitle;
		}
		
		public function getThumbnail() {
			return $this->thumbnail;
		}
	}