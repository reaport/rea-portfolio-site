<?php
	class ThumbnailManager {
		private $thumbnails = array();
		
		/**
		* Sets the thumbnail for the specified subdomain. If a thumbnail has already been specified
		* it will overwrite the old one.
		* $imageData is a string of binary jpg data.
		*/
		public function setThumbnail($domainName, $imageData) {
			$this->thumbnails[$domainName] = $imageData;
		}
		
		/**
		* Will write the thumbnail data to the specified folder. Any jpg already in the folder will
		* be removed or overwritten.
		*/
		public function saveScreenShots($thumbnailDir) {
			$this->cleanDir($thumbnailDir);
			
			foreach($this->thumbnails as $domain => $data) {
				$this->saveThumbnail($thumbnailDir, $domain, $data);
			}
		}
		
		private function cleanDir($thumbnailDir) {
			if(!file_exists($thumbnailDir)) {
				// make sure the thumbnail dir exists.
				mkdir($thumbnailDir, 0777, true);
			} else {
				// delete .jpg files for domains that do not exist
				foreach(glob($thumbnailDir . '*.jpg') as $file) {
					if(is_file($file) && !isset($this->thumbnails[$file])) {
						unlink($file);
					}
				}
			}
		}
		
		private function saveThumbnail($thumbnailDir, $domainName, $data) {
			$filePath = $thumbnailDir . $domainName . '.jpg';
			
			file_put_contents($filePath, $data);
		}
	}