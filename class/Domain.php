<?php
	class Domain {
		private $metaData;
		
		public function setName($name) {
			$this->metaData['name'] = $name;
		}
		
		public function getName() {
			return $this->metaData['name'];
		}
		
		public function setUrl($url) {
			// ensure a url scheme
			if(parse_url($url, PHP_URL_SCHEME) === null) {
				$url = 'http://' . $url;
			}
			
			$this->metaData['url'] = $url;
		}
		
		public function getUrl() {
			return $this->metaData['url'];
		}
		
		public function setClientName($name) {
			$this->metaData['clientName'] = $name;
		}
		
		public function getClientName() {
			return $this->metaData['clientName'];
		}
		
		public function setPageTitle($title) {
			$this->metaData['title'] = $title;
		}
		
		public function getPageTitle() {
			return $this->metaData['title'];
		}
		
		public function setLocation(Location $location) {
			$this->metaData['loc'] = $location;
		}
		
		public function getLocation() {
			return $this->metaData['loc'];
		}
		
		public function setThumbnail($url) {
			$this->metaData['thumbnail'] = $url;
		}
		
		public function getThumbnail() {
			return $this->metaData['thumbnail'];
		}
		
		public function store(&$storage) {
			$storage[] = array(
				'name' => $this->getName(),
				'url' => $this->getUrl(),
				'clientName' => $this->getClientName(),
				'pageTitle' => $this->getPageTitle(),
				'location' => (string) $this->getLocation(),
				'thumbnail' => $this->getThumbnail(),
			);
		}
		
		public function load($storage) {
			$this->setName($storage['name']);
			$this->setUrl($storage['url']);
			$this->setClientName($storage['clientName']);
			$this->setLocation(Location::fromString($storage['location']));
			$this->setPageTitle($storage['pageTitle']);
			$this->setThumbnail($storage['thumbnail']);
		}
	}