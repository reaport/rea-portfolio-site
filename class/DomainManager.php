<?php
	class DomainManager {
		private $domains;
		
		public function setDomain(Domain $domain) {
			$this->domains[$domain->getName()] = $domain;
		}
		
		/**
		* Removes all domains that do not contain $query in their name, clientName or page title.
		*/
		public function filterByQuery($query) {
			foreach($this->domains as $domain) {
				if(!(stristr($domain->getName(), $query)
				|| stristr($domain->getClientName(), $query)
				|| stristr($domain->getPageTitle(), $query))) {
					unset($this->domains[$domain->getName()]);
				}
			}
		}
		
		/**
		* Removes all domains that do not match $location in their location.
		*/
		public function filterByLocation(Location $location) {
			if($location == new Location(Location::UNKNOWN)) return;
			
			foreach($this->domains as $key => $domain) {
				if($domain->getLocation() != $location) {
					unset($this->domains[$domain->getName()]);
				}
			}
		}
		
		/**
		* Sorts the domains by the client's last name.
		*/
		public function sortArrays() {
			uasort($this->domains, function($a, $b) {
				$a = $a->getClientName();
				$b = $b->getClientName();
				
				// if the clientname contains a period, subtring it.
				if(strpos($a, '.') !== false) $a = strstr($a, '.');
				if(strpos($b, '.') !== false) $b = strstr($b, '.');
								
				if($a == $b) return 0;
				return ($a < $b) ? -1 : 1;
			});
		}
		
		/**
		* Returns all the domains that the domain manager has loaded.
		*/
		public function getDomains($start = 0, $length = null) {
			return array_slice($this->domains, $start, $length, true);
		}
		
		/**
		* Searches the loaded domains and returns the domain where getName matches $domainName.
		* Returns null if no domain is loaded with that name.
		*/
		public function getDomainFromName($domainName) {
			foreach($this->domains as $domain) {
				if($domain->getName() === $domainName) return $domain;
			}
			
			return null;
		}
		
		/**
		* Json encodes the domains and stores them to a file.
		*/
		public function saveDomainsToFile($filePath) {
			$out = array();
			
			foreach($this->domains as $domain) {
				$domain->store($out);
			}
			
			file_put_contents($filePath, json_encode($out));
		}
		
		/**
		 * Loads domains from a json encoded file.
		 */
		public function loadDomainsFromFile($filePath) {
			$metadata = json_decode(file_get_contents($filePath), true);
			
			foreach($metadata as $domainArray) {
				$domain = new Domain();
				$domain->load($domainArray);
				
				$this->setDomain($domain);
			}
		}
	}