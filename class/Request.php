<?php
	class Request {
		public static function get($name, $default = null) {
			return (isset($_GET[$name]) && $_GET[$name] !== '') ? $_GET[$name] : $default;
		}
		
		public static function cookie($name, $default = null) {
			return (isset($_COOKIE[$name]) && $_COOKIE[$name] !== '') ? $_COOKIE[$name] : $default;
		}
	}