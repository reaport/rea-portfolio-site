<?php
	include 'config.php';
	
	// autoloader
	function autoload($cls) {		
		$filepath = "class/{$cls}.php";
		
		if(is_file($filepath)) include $filepath;
	}
	
	spl_autoload_register('autoload', true);
	
	// make php exception heavy
	set_error_handler(function($errId, $errMsg, $errFile, $errLine) {
		throw new ErrorException($errMsg, 0, $errId, $errFile, $errLine);
	});
	
	error_reporting(E_ALL);
	
	// install an exception handler so the user will get a nice 500 page instead of a garbled page.
	if($config['DEBUG_MODE']) {
		echo '<p>!Website is running in debug mode!</p>';
	} else {
		set_exception_handler(array('ErrorHandler', 'HandleException'));
	}