<?php
	include 'init.php';
	
	// Load domains
	$domainManager = new DomainManager();
	$domainManager->loadDomainsFromFile($config['METADATA_FILE']);
	if(Request::get('loc') != null)	$domainManager->filterByLocation(Location::fromString(Request::get('loc')));
	if(Request::get('q') != null)	$domainManager->filterByQuery(Request::get('q'));
	$domainManager->sortArrays();
	
	// Create content
	$content = new Template();
	$content->page_number = max(1, intval(Request::get('page', '1')));
	$content->page_domainCount = $config['PAGINATION_ITEMS_PER_PAGE'];
	$content->page_domainOffset = ($content->page_number - 1) * $content->page_domainCount;
	$content->page_domainTotal = count($domainManager->getDomains());
	$content->page_total = ceil($content->page_domainTotal / $content->page_domainCount);
	$content->domains = $domainManager->getDomains($content->page_domainOffset, $content->page_domainCount);
	$content->messages = (Request::get('q') != null) ? '<p>Zoekresultaten voor "' . Request::get('q') . '":</p>' : '';	
	$content->thumbnailDir = $config['THUMBNAIL_DIR'];
	$content->viewClass = (Request::cookie('view', 'grid')) == 'grid' ? 'grid' : 'list';
	
	// Create menu
	$menu = new Template();
	$menu->lwdButtonClass = Request::get('loc') == 'L' ? 'pressed' : '';
	$menu->grnButtonClass = Request::get('loc') == 'G' ? 'pressed' : '';
	$menu->query = Request::get('q', '');
	$menu->loc = Request::get('loc', '');
	$menu->viewClass = (Request::cookie('view', 'grid')) == 'grid' ? 'list' : 'grid';
	
	// Create main template
	$index = new Template();
	$index->menu = $menu->render('template/menu.html');
	$index->content = $content->render('template/index.html');
	
	// Output the thing
	echo $index->render('template/base.html');